package com.mvp.cartoes.cliente.service;

import com.mvp.cartoes.cliente.models.Client;
import com.mvp.cartoes.cliente.repository.ClientRepository;
import com.mvp.cartoes.cliente.utils.exceptions.ClienteNotFoundException;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    public Client create(Client client) {
        return clientRepository.save(client);
    }

    public Client getById(Long id) {
        Optional<Client> byId = clientRepository.findById(id);

        if(!byId.isPresent()) {
            throw new ClienteNotFoundException();
        }

        return byId.get();
    }

}
