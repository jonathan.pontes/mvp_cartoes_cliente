package com.mvp.cartoes.cliente.repository;

import com.mvp.cartoes.cliente.models.Client;
import org.springframework.data.repository.CrudRepository;

public interface ClientRepository extends CrudRepository<Client, Long> {

}
