package com.mvp.cartoes.cliente.controller;

import com.mvp.cartoes.cliente.models.Client;
import com.mvp.cartoes.cliente.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Client create(@RequestBody Client client) {
        return clientService.create(client);
    }

    @GetMapping("/{id}")
    public Client getById(@PathVariable Long id) {
        System.out.println("Buscaram um carro de id " + id + " em " + System.currentTimeMillis());
        return clientService.getById(id);
    }

}
